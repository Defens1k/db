package main

import (
    "fmt"
    "context"
    "database/sql"
    _ "github.com/lib/pq"
    _ "reflect"
)

const (
    host = "postgres"
    port = 5432
    user = "admin-user"
    password = "admin"
    dbname = "test"
)

type Db struct {
    db * sql.DB
    ctx * context.Context
}

func newDb(ctx * context.Context) * Db {
    var db Db
    db.ctx = ctx
    return &db
}

func (this * Db) connect() error {
    psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
        host,
        port,
        user,
        password,
        dbname)
    var err error
    this.db, err = sql.Open("postgres", psqlconn)
    return err
}

func (this * Db) disconnect() {
    this.db.Close()
}

func (this * Db) registerOperation(operation_id string) error{
    tx, err := this.db.BeginTx(*this.ctx, nil)
    if err != nil {
        return err
    }
    checkOperation := fmt.Sprintf("SELECT count(*) FROM operations WHERE operation_id = '%s';", operation_id)
    row := this.db.QueryRow(checkOperation)

    var count int = 0
    err = row.Scan(&count)
    if err != nil {
        fmt.Println(err)
        tx.Rollback()
        return err
    }
    if count != 0 {
        tx.Rollback()
        return fmt.Errorf("operation already registred")

    }
    insertDynStmt := fmt.Sprintf("INSERT INTO operations (operation_id) VALUES('%s');", operation_id)
    _, err = this.db.Exec(insertDynStmt)
    if err != nil {
        fmt.Println(err)
        tx.Rollback()
        return err
    }

    tx.Commit()
    return nil
}

func (this * Db) registerUser(userId string) error {
    tx, err := this.db.BeginTx(*this.ctx, nil)
    if err != nil {
        fmt.Println(err)
        return err
    }
    fmt.Println(userId)
    checkOperation := fmt.Sprintf("SELECT COUNT(*) FROM users WHERE id = '%s' ;", userId)
    fmt.Println(checkOperation)
    row := this.db.QueryRow(checkOperation)


    var count int = 0
    err = row.Scan(&count)
    if err != nil {
        fmt.Println(err)
        tx.Rollback()
        return err
    }

    if err != nil {
        fmt.Println(err)
        tx.Rollback()
        return err
    }
    if count != 0 {
        tx.Rollback()
        return fmt.Errorf("user exists")
    }
    insertDynStmt := fmt.Sprintf("INSERT INTO users (id) VALUES('%s');", userId)
    _, err = this.db.Exec(insertDynStmt)
    if err != nil {
        fmt.Println(err)
        tx.Rollback()
        return err
    }

    tx.Commit()
    return nil
}


func (this * Db) updateBalance(userId string, balanceDelta int) error {
    tx, err := this.db.BeginTx(*this.ctx, nil)
    if err != nil {
        return err
    }
    checkOperation := fmt.Sprintf("SELECT balance FROM users WHERE id = '%s';", userId)
    row := this.db.QueryRow(checkOperation)

    var balance int
    err = row.Scan(&balance)
    if err != nil {
        tx.Rollback()
        return err
    }
    if  balance + balanceDelta < 0 {
        tx.Rollback()
        return fmt.Errorf("balance can not be less than zero")

    }

    insertDynStmt := fmt.Sprintf("UPDATE users SET balance = %d WHERE id = '%s';", balance + balanceDelta, userId)
    _, err = this.db.Exec(insertDynStmt)
    if err != nil {
        tx.Rollback()
        return err
    }

    tx.Commit()
    return nil
}

func (this * Db) updateOperationStatus(operationId string, status string) error {
    insertDynStmt := fmt.Sprintf("UPDATE operations SET status = '%s' WHERE operation_id = '%s';", status , operationId)
    _, err := this.db.Exec(insertDynStmt)
    if err != nil {
        return err
    }

    return nil
}
