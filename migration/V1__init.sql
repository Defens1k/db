CREATE TABLE users(
   id TEXT NOT NULL,
   balance INT NOT NULL DEFAULT 0
);


CREATE TYPE operation_status AS ENUM ('OK', 'FAILED', 'PROCESSING');

CREATE TABLE operations(
    operation_id TEXT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    status operation_status NOT NULL DEFAULT 'PROCESSING'
);
