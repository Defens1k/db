package main

import (
    "context"
    "fmt"
    _ "reflect"
)

const (
    key = "key"
)


type BalanceConsumer struct {
    broker * Kafka
    db * Db
    koef int
}

func newBalanceConsumer(ctx * context.Context, requestsTopic string, responsesTopic string, koef int) * BalanceConsumer {
    var consumer BalanceConsumer
    consumer.broker = newKafka(ctx, requestsTopic, responsesTopic)
    consumer.db = newDb(ctx)
    consumer.koef = koef
    err := consumer.db.connect()
    if err != nil {
        return nil
    }
    return &consumer
}

type Request struct {
    operationId string
    userId string
    balanceDelta int
}

func parseRequest(message string) (error, Request) {
    var req Request
    fmt.Println("message in parse")
    scanned, err := fmt.Sscanf(message, "%s %s %d", &req.operationId, &req.userId, &req.balanceDelta)
    if scanned != 3 || err != nil {
        fmt.Println("bad Format")
        return fmt.Errorf("bad format"), req
    }
    fmt.Println("good Format")
    return nil, req
}

func (this * BalanceConsumer) work(message string) {
    fmt.Println("message in work" + message)
    err, req := parseRequest(message)
    if err != nil {
        fmt.Println("Can not parse")
        this.broker.sendMessage(key, "Can not parse: " + message)
        return
    }

    err = this.db.registerOperation(req.operationId)
    if err != nil {
        this.broker.sendMessage(key, req.operationId + err.Error())
        return
    }
    var status string = "FAILED"
    defer this.db.updateOperationStatus(req.operationId, status)

    err = this.db.updateBalance(req.userId, req.balanceDelta * this.koef)
    if err != nil {
        this.broker.sendMessage(key, req.operationId + err.Error())
        return
    }
    status = "OK"
    this.broker.sendMessage(key, "Operation with id: " + req.operationId + " succsesfuly passed")
}

func (this * BalanceConsumer) consume() {
    for {
        message := this.broker.readMessage()
        fmt.Println("message from broker: " + message)
        go this.work(message)
    }
}

func (this * BalanceConsumer) disconnect() {
    this.db.disconnect()
}


type UserConsumer struct {
    broker * Kafka
    db * Db
}

func newUserConsumer(ctx * context.Context, requestsTopic string, responsesTopic string) * UserConsumer {
    var consumer UserConsumer
    consumer.broker = newKafka(ctx, requestsTopic, responsesTopic)
    consumer.db = newDb(ctx)
    err := consumer.db.connect()
    if err != nil {
        return nil
    }
    return &consumer
}

func (this * UserConsumer) work(message string) {
    fmt.Println(message)
    result := this.db.registerUser(message)
    userKey := "User registred: "
    if result != nil {
        userKey = "Error while trying register user: "
    }
    this.broker.sendMessage(key, userKey + message)
}

func (this * UserConsumer) consume() {
    for {
        message := this.broker.readMessage()
        fmt.Println("message from broker: " + message)
        go this.work(message)
    }
}

func (this * UserConsumer) disconnect() {
    this.db.disconnect()
}

