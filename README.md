##### Использованные технологии
- golang
- postgresql
- kafka
- docker-compose
- flyway

##### Для взаимодействия с микросервисам доступны следующие топики для увеличения или уменьшения баланса и на создания пользователей
```"IncreaseBalanceRequests"
"IncreaseBalanceResponses"
"DecreaseBalanceRequests"
"DecreaseBalanceResponses"
"CreateUserRequests"
"CreateUserResponses"
```

#### Создание позьзователя
На любое сообщение в топике CreateUserRequests сервис создаст пользователя с таким айди

#### Начисление средств
Создаем сообщение в топике IncreaseBalanceRequests в формате "operationId userId balance", где
- operationId - строка - должен быть уникальным для каждой операции
- userId - строка - айди ранее созданного позьзователя, баланс которого мы меняем
- balance - число рублей, на сколько изменить баланс
