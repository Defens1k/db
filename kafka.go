package main

import (
    "context"
    "fmt"
    "github.com/segmentio/kafka-go"
)

const (
    brokerAddress = "kafka:29092"
)


type Kafka struct {
    writer * kafka.Writer
    reader * kafka.Reader
    ctx * context.Context
}

func newKafka(ctx * context.Context, requestsTopic string, responsesTopic string) * Kafka {
    var object Kafka
    object.ctx = ctx
    object.writer = kafka.NewWriter(kafka.WriterConfig{
        Brokers: []string{brokerAddress},
        Topic:   responsesTopic,
    })
    object.reader = kafka.NewReader(kafka.ReaderConfig{
        Brokers: []string{brokerAddress},
        Topic:   requestsTopic,
        GroupID: "my-group",
    })
    return &object
}

func (this * Kafka) sendMessage(key string, value string) {
    fmt.Println("writing msg: ", value)
    err := this.writer.WriteMessages(*this.ctx, kafka.Message{
        Key: []byte(key),
        Value: []byte(value),
    })
    if err != nil {
        fmt.Println("could not write message " + err.Error())
    }
}

func (this * Kafka) readMessage() string {
    msg, err := this.reader.ReadMessage(*this.ctx)
    if err != nil {
        fmt.Println("could not read message " + err.Error())
    }
    fmt.Println("received: ", string(msg.Value))
    return string(msg.Value)
}


