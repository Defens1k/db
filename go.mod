module main

go 1.13

require (
	github.com/klauspost/compress v1.14.2
	github.com/lib/pq v1.10.5
	github.com/pierrec/lz4/v4 v4.1.14
	github.com/segmentio/kafka-go v0.4.31
)
