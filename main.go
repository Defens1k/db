package main

import (
     "context"
    _ "fmt"
    _ "reflect"
)

const (
    increaseBalanceRequests          = "IncreaseBalanceRequests"
    increaseBalanceResponses         = "IncreaseBalanceResponses"
    decreaseBalanceRequests          = "DecreaseBalanceRequests"
    decreaseBalanceResponses         = "DecreaseBalanceResponses"
    createUserRequests               = "CreateUserRequests"
    createUserResponses              = "CreateUserResponses"
)


func main() {
    ctx := context.Background()
    userConsumer := newUserConsumer(&ctx, createUserRequests, createUserResponses)
    increaseConsumer := newBalanceConsumer(&ctx, increaseBalanceRequests, increaseBalanceResponses, 1)
    decreaseConsumer := newBalanceConsumer(&ctx, decreaseBalanceRequests, decreaseBalanceResponses, -1)
    if userConsumer == nil || decreaseConsumer == nil || increaseConsumer == nil  {
        panic("can not create consumer")
    }
    defer userConsumer.disconnect()
    defer increaseConsumer.disconnect()
    defer decreaseConsumer.disconnect()

    go increaseConsumer.consume()
    go decreaseConsumer.consume()
    userConsumer.consume()

}

